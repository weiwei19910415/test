<?php 
	require_once("../config/connect.php");
	require_once("../config/myAPI.php");
?>
<!DOCTYPE html>
<html>

<head>
	<title><?=$rs['pageTitle']?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/jquery-ui-1.10.3.custom.css" rel="stylesheet" media="screen">
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.js"></script>
	<script src="../js/bootstrap.min.js">
        $('.dropdown-toggle').dropdown();
    </script>
    <script src="../js/randimg.js"></script>





    <style>
  #gallery { float: left; width: 65%; min-height: 12em; }
  .gallery.custom-state-active { background: #eee; }
  .gallery li { float: left; width: 96px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
  .gallery li h5 { margin: 0 0 0.4em; cursor: move; }
  .gallery li a { float: right; }
  .gallery li a.ui-icon-zoomin { float: left; }
  .gallery li img { width: 100%; cursor: move; }
 
    #do { float: left; width: 25%; min-height: 18em; padding: 1%; }
    #do h4 { line-height: 16px; margin: 0 0 0.4em; }
    #do h4 .ui-icon { float: left; }
    #do .gallery h5 { display: none; }

    #todo { float: left; width: 25%; min-height: 18em; padding: 1%; }
    #todo h4 { line-height: 16px; margin: 0 0 0.4em; }  
    #todo h4 .ui-icon { float: left; }
    #todo .gallery h5 { display: none; }

    #done { float: left; width: 25%; min-height: 18em; padding: 1%; }
    #done h4 { line-height: 16px; margin: 0 0 0.4em; }
    #done h4 .ui-icon { float: left; }
    #done .gallery h5 { display: none; }

  </style>
  <script>
  $(function() {
    // there's the gallery and the trash
    var $gallery = $( "#gallery" ),
      $trash = $( "#do" );
 
    // let the gallery items be draggable
    $( "li", $gallery ).draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
 
    // let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: "#gallery > li",
      activeClass: "ui-state-highlight",
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: "#do li",
      activeClass: "custom-state-active",
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // image deletion function
    var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
    function deleteImage( $item ) {
      $item.fadeOut(function() {
        var $list = $( "ul", $trash ).length ?
          $( "ul", $trash ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
 
        $item.find( "a.ui-icon-trash" ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $item
            .animate({ width: "48px" })
            .find( "img" )
              .animate({ height: "36px" });
        });
      });
    }
 
    // image recycle function
    var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
    function recycleImage( $item ) {
      $item.fadeOut(function() {
        $item
          .find( "a.ui-icon-refresh" )
            .remove()
          .end()
          .css( "width", "96px")
          .append( trash_icon )
          .find( "img" )
            .css( "height", "72px" )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( "href" ),
        title = $link.siblings( "img" ).attr( "alt" ),
        $modal = $( "img[src$='" + src + "']" );
 
      if ( $modal.length ) {
        $modal.dialog( "open" );
      } else {
        var img = $( "<img alt='" + title + "' width='384' height='288' style='display: none; padding: 8px;' />" )
          .attr( "src", src ).appendTo( "body" );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }
 
    // resolve the icons behavior with event delegation
    $( "ul.gallery > li" ).click(function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( "a.ui-icon-trash" ) ) {
        deleteImage( $item );
      } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
        viewLargerImage( $target );
      } else if ( $target.is( "a.ui-icon-refresh" ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
  });
  </script>






</head>

<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="../suite_center/index.php">Kanban Boards System</a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> User&Group Management <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="../suite_member/index.php" data-toggle="modal">User Management</a></li>
                                <li><a href="../suite_group/index.php">Group Management</a></li>
                            </ul>
                        </li>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Project Management <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li class="nav-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                    <p class="navbar-text pull-right">
                        Hello!!! <a href="#" class="navbar-link"><?=$_SESSION["name"]?></a>
                        <a href="../logout.php" class="navbar-link">Logout</a>
                    </p>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>




        <div class="ui-widget ui-helper-clearfix">
 
        <ul id="gallery" class="gallery ui-helper-reset ui-helper-clearfix">
          <li class="ui-widget-content ui-corner-tr">
            <h5 class="ui-widget-header">High Tatras</h5>
          </li>
          <li class="ui-widget-content ui-corner-tr">
            <h5 class="ui-widget-header">High Tatras 2</h5>
          </li>
          <li class="ui-widget-content ui-corner-tr">
            <h5 class="ui-widget-header">High Tatras 3</h5>
          </li>
          <li class="ui-widget-content ui-corner-tr">
            <h5 class="ui-widget-header">High Tatras 4</h5>
          </li>
        </ul>
         
        <div id="do" class="ui-widget-content ui-state-default">
          <h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">Do</span> Do</h4>
        </div>

        <div id="todo" class="ui-widget-content ui-state-default">
          <h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">To Do</span> To Do</h4>
        </div>

        <div id="done" class="ui-widget-content ui-state-default">
          <h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">Done</span> Done</h4>
        </div>
 
        </div>



    </div>
</body>

</heml>