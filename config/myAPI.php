<?php
	//取得現在日期時間
	require_once("myAPI/getNowDateTime.inc.php");
	
	//彈出視窗以及導向
	require_once("myAPI/alertBox.inc.php");	
	
	//selcted的外掛
	require_once("myAPI/selected.inc.php");	
	

	//換頁API 
	require_once("myAPI/pageCreater.inc.php");	
	
	//台灣簡訊 查詢簡訊餘額
	//require_once("myAPI/smsCounter.inc.php");
	
	//台灣簡訊 發送簡訊
	//require_once("myAPI/smsSend.inc.php");

	//文章斷行以及空白
	require_once("myAPI/textReplaces.inc.php");
	
	//檔案格式類型判
	//require_once("myAPI/checkFileFormat.inc.php");

	//清除tag
	require_once("myAPI/clearTag.inc.php");
	
	//確認是否登入
	require_once("myAPI/checkLogin.inc.php");
	
	//截字
	require_once("myAPI/cutFont.inc.php"); 
	
	//網頁安全性守衛-
	require_once("myAPI/guard.inc.php"); 


?>