<?php
//上傳圖檔
function uploadImg2($files,$path,$autosize,$proportional,$autosizeW,$autosizeH,$thumbnail,$thumbnailPath,$thumbnailW,$thumbnailH){
//path 主要路徑
//autosize 如尺寸大於autosizeH則進行縮圖 0是1否
//thumbnail是否產生縮圖 0否1是
//thumbnailPath 縮圖位置,thumbnailH縮圖長,thumbnailW縮圖寬
//$file_format檔案格式
//$proportional 比例0無1有
//$count 批次次
//file是否陣列很重要[]
	/*echo "Filename: ".$_FILES['file']['name'][$count]."<br>";
	echo "Temporary Name: ".$_FILES['file']['tmp_name'][$count]."<br>";
	echo "Size: ".$_FILES['file']['size'][$count]."<br>";
	echo "Type: ".$_FILES['file']['type'][$count]."<br>";*/
	
	/*if($_FILES[$files]['error'][$count] > 0){
		switch($_FILES[$files]['error'][$count]){
			case 1 : die("檔案大小超出 php.ini:upload_max_filesize 限制");
			case 2 : die("檔案大小超出 MAX_FILE_SIZE 限制");
			case 3 : die("檔案僅被部分上傳");
			case 4 : die("檔案未被上傳");
		}
	}*/ 
		
		$DestDIR = $path;//目錄		
		
		
		$file_explode=explode(".",$_FILES[$files]['name']);
		$file_explode = $file_explode[count($file_explode)-1]; //求出附檔名
	
		$ServerFilename =date("YmdHis")."-".rand().".".$file_explode;//產生檔名		
		copy($_FILES[$files]['tmp_name'] , $DestDIR . "/" . $ServerFilename );//存到路徑
			
		
		//判斷是否縮圖				
		if($autosize==1){			
			$files=$DestDIR."/".$ServerFilename;
			$srcInfo  = pathinfo($files);//圖檔資訊
			$srcSize  = getimagesize($files); //圖檔大小	
			
			$srcRatio  = $srcSize[0]/$srcSize[1];//原尺寸 計算寬/高            
			$destRatio = $autosizeW/$autosizeH;//最大尺寸		
				
					
			//判斷是否大於
			if($srcSize[0]>$autosizeW || $srcSize[1]>$autosizeH){
				//判斷是否比例
				if ($destRatio > $srcRatio) {
					$destSize[1] = $autosizeH;
					$destSize[0] = $autosizeH*$srcRatio;
				} else {
					$destSize[0] = $autosizeW;
					$destSize[1] = $autosizeW/$srcRatio;
				}				
			}else{
				$destSize[0] = $srcSize[0];
				$destSize[1] = $srcSize[1];				
			}
			
					
			$destImage = imagecreatetruecolor($destSize[0],$destSize[1]);
			 //如果是png採取
			 if($srcSize[2]==3){
				
				$color=imagecolorallocate($destImage,255,255,255);
				imagecolortransparent($destImage,$color);
				imagefill($destImage,0,0,$color);				 
			 }
			 
			 //建立檔案類型
			  switch ($srcSize[2]) {
                case 1: $srcImage = imagecreatefromgif($files); break;
                case 2: $srcImage = imagecreatefromjpeg($files); break;
                case 3: $srcImage = imagecreatefrompng($files); break;
                default: return false; break;
            }
			//合併			
			imagecopyresampled($destImage, $srcImage, 0, 0, 0, 0,$destSize[0],$destSize[1],
                                $srcSize[0],$srcSize[1]);
			//存檔
			 switch ($srcSize[2]) {
                case 1:	imagegif($destImage,$files); break;
				case 2: imagejpeg($destImage,$files,100); break;
                case 3: imagepng($destImage,$files); break;
            }
			
			imagedestroy($destImage);				
								
		}
		//判斷是否產生小圖		
		if($thumbnail==1){
			$sDestDIR = $thumbnailPath;//目錄		
		
			$files=$DestDIR."/".$ServerFilename;
			$srcInfo  = pathinfo($files);//圖檔資訊
			$srcSize  = getimagesize($files); //圖檔大小				
						
			$srcRatio  = $srcSize[0]/$srcSize[1];//原尺寸 計算寬/高            
			$destRatio = $thumbnailW/$thumbnailH;//最大尺寸		
				
					//判斷比例
				
			if ($destRatio > $srcRatio) {
            	$destSize[1] = $thumbnailH;
            	$destSize[0] = $thumbnailH*$srcRatio;
            } else {
               	$destSize[0] = $thumbnailW;
               	$destSize[1] = $thumbnailW/$srcRatio;
            }	
			
			
			$destImage = imagecreatetruecolor($destSize[0],$destSize[1]);
			 //如果是png採取
			 if($srcSize[2]==3){
				
				$color=imagecolorallocate($destImage,255,255,255);
				imagecolortransparent($destImage,$color);
				imagefill($destImage,0,0,$color);				 
			 }
			 
			 //建立檔案類型
			  switch ($srcSize[2]) {
                case 1: $srcImage = imagecreatefromgif($files); break;
                case 2: $srcImage = imagecreatefromjpeg($files); break;
                case 3: $srcImage = imagecreatefrompng($files); break;
                default: return false; break;
            }
			//合併			
			imagecopyresampled($destImage, $srcImage, 0, 0, 0, 0,$destSize[0],$destSize[1],
                                $srcSize[0],$srcSize[1]);
			//存檔
			 switch ($srcSize[2]) {
                case 1:	imagegif($destImage,$sDestDIR."/".$ServerFilename); break;
				case 2: imagejpeg($destImage,$sDestDIR."/".$ServerFilename,100); break;
                case 3: imagepng($destImage,$sDestDIR."/".$ServerFilename); break;
            }
			
			imagedestroy($destImage);			
											
		}
						
		
	
	return $ServerFilename;
}

?>