<?php
//上傳檔案
function uploadFile($file,$path,$file_format){
//file 蘭位名稱
//path 主要路徑
//$file_format檔案格式
//$count 批次次數	
	
	if(is_uploaded_file($_FILES[$file]['tmp_name'])){
		$DestDIR = $path;//目錄
		
		if(!is_dir($DestDIR) || !is_writeable($DestDIR)){
			die($DestDIR."目錄不存在或無法寫入");
		}
	
		$checkFile='';
		
		$file_explode=explode(".",$_FILES[$file]['name']);
		$file_explode = $file_explode[count($file_explode)-1]; //求出附檔名
		$file_formats=explode(",",$file_format);//把檔案格式分解到陣列
	
		//判斷檔案格式
		foreach($file_formats as $val){
			if(!strcasecmp($file_explode,$val)){			
				$checkFile="y";											
			}
		}			
				
				
		//確認檔案格式
		if ($checkFile=='y'){		
			$ServerFilename =date("YmdHis")."-".rand().".".$file_explode;	//產生檔名
			copy($_FILES['file']['tmp_name'] , $DestDIR . "/" . $ServerFilename );		
			return $ServerFilename;
		}
	}
}



?>