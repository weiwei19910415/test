﻿<?php
require_once("phpmailer/class.phpmailer.php");

function phpMailer($fromPeople,$fromMail,$toMail,$mailTitle,$mailContent){

/*
$fromPeople 寄件者名稱
$fromMail 寄件者mail
$toMail 收件者
$mailTitle 主指
$mailContent 內容
*/

//宣告一個PHPMailer物件
	$mail = new PHPMailer();

//設定使用SMTP發送
	$mail->IsSMTP();

//指定SMTP的服務器位址
	$mail->Host = "localhost";
//設定SMTP服務的POST
	$mail->Port = 25;

//設定為安全驗證方式
	$mail->SMTPAuth = false;

//SMTP的帳號
	$mail->Username = "wd.3d.mynet@gmail.com";
//SMTP的密碼
	$mail->password = "viewsonic";

//寄件人Email
	$mail->From = $fromMail;
//寄件人名稱
	$mail->FromName = $fromPeople;

//收件人Email
	$mail->AddAddress($toMail);
//設定收件人的另一種格式("Email","收件人名稱")
//$mail->AddAddress("justin@recyclesources.com","阿豪");
//設定密件副本
//$mail->AddBCC("bignostriltao@gmail.com");

//回信Email及名稱
//$mail->AddReplyTo("xuhao@so-net.net.tw", "大鼻子");

//設定信件字元編碼
	$mail->CharSet="utf-8";
//設定信件編碼，大部分郵件工具都支援此編碼方式
	$mail->Encoding = "base64";
//設置郵件格式為HTML
	$mail->IsHTML(true);
//每50自斷行
//$mail->WordWrap = 50;

//傳送附檔
//$mail->AddAttachment("upload/temp/filename.zip");
//傳送附檔的另一種格式，可替附檔重新命名
//$mail->AddAttachment("upload/temp/filename.zip", "newname.zip");

//郵件標題
	$mail->Subject=$mailTitle;
//郵件內容
	$mail->Body =$mailContent;

//附加內容
//$mail->AltBody = '這是附加的信件內容';

//寄送郵件
$result=$mail->Send();
return $result;

}

?>