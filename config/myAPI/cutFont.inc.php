<?php
//截字
function cutFont($text,$count,$more,$links){
	//$text 文字
	//$count 截字的字數
	//$more 是否出先more 0/1
	//$links 網址
	//需要設定.more的css
	if(mb_strlen($text,'utf8')>$count){
		
		if($more==1){
			$mlink="<span class='more'><a href='".$links."'>...more</a></span>";
		}else{
			$mlink="...";
		}	
		
		return "<a href='".$links."'>".mb_substr(strip_tags($text),0,$count,'UTF-8')."</a>".$mlink;
	}else{
		return "<a href='".$links."'>".$text."</a>";
	}	
}

?>