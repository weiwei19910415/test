<?php
//上傳圖檔
function uploadImgGD($files,$path,$autosize,$proportional,$autosizeW,$autosizeH,$thumbnail,$thumbnailPath,$thumbnailW,$thumbnailH,$file_format,$count){
//path 主要路徑
//autosize 如尺寸大於autosizeH則進行縮圖 0是1否
//thumbnail是否產生縮圖 0否1是
//thumbnailPath 縮圖位置,thumbnailH縮圖長,thumbnailW縮圖寬
//$file_format檔案格式
//$proportional 比例0無1有
//$count 批次次數
	
	/*echo "Filename: ".$_FILES['file']['name'][$count]."<br>";
	echo "Temporary Name: ".$_FILES['file']['tmp_name'][$count]."<br>";
	echo "Size: ".$_FILES['file']['size'][$count]."<br>";
	echo "Type: ".$_FILES['file']['type'][$count]."<br>";*/
	
	/*if($_FILES[$files]['error'][$count] > 0){
		switch($_FILES[$files]['error'][$count]){
			case 1 : die("檔案大小超出 php.ini:upload_max_filesize 限制");
			case 2 : die("檔案大小超出 MAX_FILE_SIZE 限制");
			case 3 : die("檔案僅被部分上傳");
			case 4 : die("檔案未被上傳");
		}
	}*/ 
	
	if(is_uploaded_file($_FILES[$files]['tmp_name'][$count])){
		$DestDIR = $path;//目錄
		
		if(!is_dir($DestDIR) || !is_writeable($DestDIR)){
			die($DestDIR."目錄不存在或無法寫入");
		}
	
		$checkFile='';
				
		$file_explode=explode(".",$_FILES[$files]['name'][$count]);
		$file_explode = $file_explode[count($file_explode)-1]; //求出附檔名
		$file_formats=explode(",",$file_format);//把檔案格式分解到陣列
		
		//判斷檔案格式
		foreach($file_formats as $val){
			
			if(!strcasecmp($file_explode,$val)){				
				$checkFile="y";												
			}
			
		}	
	
		//exit;
		//確認檔案格式
		if ($checkFile=='y'){			
			$ServerFilename =date("YmdHis")."-".rand().".".$file_explode;	//產生檔名
			copy($_FILES[$files]['tmp_name'][$count] , $DestDIR . "/" . $ServerFilename );			
			
			if(strcasecmp($file_explode,'swf')){
			
				//判斷是否縮圖				
				if($autosize==1){
				
					$img= new Imagick();
					$img->readImage($DestDIR."/".$ServerFilename);				
					
					$img_w=$img->getImageWidth();
					$img_h=$img->getImageHeight();				
					//判斷是否比例
					if($proportional==1){									
						if($img_w > $img_h){
							if($img_w > $autosizeW){
								$thumb_w =$autosizeW;
								$thumb_h = intval($img_h / $img_w * $autosizeW);
							}else{
								$thumb_h=$img_h;
								$thumb_w=$img_w;
							}
						}elseif($img_h > $img_w){
							if($img_h > $autosizeH){
								$thumb_h = $autosizeH;
								$thumb_w = intval($img_w / $img_h * $autosizeH);
							}else{
								$thumb_h=$img_h;
								$thumb_w=$img_w;
							}
						}else{
							$thumb_h=$img_h;
							$thumb_w=$img_w;
						}
					}else{								
						$thumb_h=$autosizeH;
						$thumb_w=$autosizeW;				
					}
					
					
					if(!strcasecmp($file_explode,'gif')){
						$img->resizeImage($thumb_w,$thumb_h,Imagick::FILTER_LANCZOS,1);
						$img->writeImage($DestDIR."/".$ServerFilename);	
					}else{
						//處理GIF
						$animation = new Imagick();           
						$animation->setFormat( "gif" );				
						$unitl = $img->getNumberImages();
						
						for ($i=0; $i<$unitl; $i++) {   
							$img->setImageIndex($i);   
							$thisimage = new Imagick();           
							$thisimage->readImageBlob($img);   
							$delay = $thisimage->getImageDelay();   
							$thisimage->resizeImage($thumb_w,$thumb_h,Imagick::FILTER_LANCZOS,1);             
							$animation->addImage($thisimage);               
							$animation->setImageDelay( $delay );
						}				
						$animation->writeImages($DestDIR."/".$ServerFilename,true);// writeImages  
							 
						//echo count($img);					
					}				
				}
				//判斷是否產生小圖		
				if($thumbnail==1){
					$sDestDIR = $thumbnailPath;//目錄
		
					if(!is_dir($sDestDIR) || !is_writeable($sDestDIR)){
						die($sDestDIR."縮圖目錄不存在或無法寫入");
					}
				
					$img= new Imagick();
					$img->readImage($DestDIR."/".$ServerFilename);				
						
					$img_w=$img->getImageWidth();
					$img_h=$img->getImageHeight();	
				
					//判斷是否比例
					if($proportional==1){	
						if($img_w >= $img_h){				
								$thumb_w =$thumbnailW;
								$thumb_h = intval($img_h / $img_w * $thumbnailW);				
						}elseif($img_h >= $img_w){				
								$thumb_h = $thumbnailH;
								$thumb_w = intval($img_w / $img_h * $thumbnailH);				
						}
					}else{
						$thumb_h = $thumbnailH;
						$thumb_w = $thumbnailW;					
					}
	
					$img->resizeImage($thumb_w,$thumb_h,Imagick::FILTER_LANCZOS,1);
					$img->writeImage($sDestDIR."/".$ServerFilename);									
				}
			}			
		}
	}
	return $ServerFilename;
}

?>