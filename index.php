<?php require_once("config/connect.php");?>
<!DOCTYPE html>
<html>
<?php 
    $sql="select pageTitle from core";
    $result=mysql_query($sql,$cn);
    $rs=mysql_fetch_array($result);
?>
<head>
	<title><?=$rs['pageTitle']?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="css/bootstrap.css" rel="stylesheet" media="screen">
    <script src="js/jquery-1.4.2.min.js"></script>
	<script src="js/bootstrap.js"></script>
    <script src="js/randimg.js"></script>
	<style type="text/css">
	body{
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
    }
    .form-signin{
    	max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
    }
    .form-signin .form-signin-heading{
    	margin-bottom: 10px;
    }
    .form-signin input[id="account"],
    .form-signin input[id="password"]{
    	font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    .form-signin input[id="code"]{
        font-size: 16px;
        height: auto;
        width: 150px;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    </style>

    <script>
        var pass='';
        $(document).ready(function(){
            randimg('config/randimg/','#randimg');
            if($.browser.version=='6.0'){
                $('#con').show('fast');
            }
        });
    </script>
</head>

<body>
	<div class="container">
		<form class="form-signin" method="post" action="login.php">
            <h2 class="form-signin-heading">Sign in</h2>
            <input type="text" name="account" id="account" class="input-block-level" placeholder="Account">
            <input type="password" name="password" id="password" class="input-block-level" placeholder="Password">
            <input type="text" name="code" id="code" class="input-block-level">
            <span id='randimg'>&nbsp;</span><a href='javascript:randimg("config/randimg/","#randimg");'><img src='images/view-refresh.png' /></a>&nbsp;
            <button class="btn btn-large btn-primary" type="submit">Sign in</button>
        </form>
	</div>
</body>

</html>